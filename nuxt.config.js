module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'JSLounge',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '説明文説明文' }
    ]
  },
  /*
  ** Modules
  */
  modules: [{
      src: '@nuxtjs/blog',
  }]
}
